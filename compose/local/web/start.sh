#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset


python manage.py collectstatic --noinput
python manage.py makemigrations
python manage.py migrate
python manage.py initadmin
python manage.py loaddata ./fixtures/db.json
python manage.py runserver 0.0.0.0:8000
