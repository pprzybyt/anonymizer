from django.contrib import admin, messages

from .models import User


def make_published(modeladmin, request, queryset):
    for q in queryset:
        q.anonymize()
    messages.info(request, 'Successfully anonymized data for selected users')


make_published.short_description = "Anonymize user and corresponding tables"


class UserAdmin(admin.ModelAdmin):
    actions = [make_published]
    list_display = ('username', 'is_anon')
    readonly_fields = ('is_anon',)


admin.site.register(User, UserAdmin)
