from django.contrib.auth.models import AbstractUser

from anonymizer.core.abstract import AnonymousModel


class User(AbstractUser, AnonymousModel):
    to_anonymize = {'username', 'email', 'first_name', 'last_name'}
