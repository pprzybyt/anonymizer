from io import StringIO

from django.contrib.auth import get_user_model
from django.core.management import call_command
from django.test import TestCase, override_settings

from anonymizer.core.encrypt import Encryptor
from anonymizer.core.models import Address, Message

User = get_user_model()


class SampleTestCase(TestCase):

    def setUp(self):
        self.encryptor = Encryptor()
        self.username = 'user'
        self.street_name = 'street_name'
        self.contact_mail = 'a@a.com'
        self.user = User.objects.create(username=self.username)

    def test_encryptor(self):
        text = 'aaa'
        encrypted = self.encryptor.encrypt(text)
        decrypted = self.encryptor.decrypt(encrypted)
        self.assertEqual(decrypted, text)

    @override_settings(SECRET_KEY="to short key")
    def test_invalid_secret(self):
        self.assertRaises(AssertionError, Encryptor)

    def test_anon_message(self):
        msg: Message = Message.objects.create(user=self.user)

        self.assertEqual(msg, Message.objects.get(user=self.user))
        self.assertTrue(not msg.is_anon)
        self.assertTrue(not self.user.is_anon)

        self.user.anonymize()

        self.assertRaises(Message.DoesNotExist, lambda: Message.objects.get(user=self.user))
        self.assertEqual(self.username, self.encryptor.decrypt(self.user.username))
        self.assertTrue(self.user.is_anon)

    def test_anon_a(self):
        addr: Address = Address.objects.create(
            user=self.user,
            street_name=self.street_name,
            contact_mail=self.contact_mail
        )

        self.assertEqual(addr, Address.objects.get(user=self.user))
        self.assertTrue(not addr.is_anon)
        self.assertTrue(not self.user.is_anon)

        self.user.anonymize()
        addr = Address.objects.get(user=self.user)
        self.assertEqual(self.username, self.encryptor.decrypt(self.user.username))

        for name in addr.to_clean:
            self.assertTrue(not getattr(addr, name))

        self.assertEqual(self.contact_mail, self.encryptor.decrypt(addr.contact_mail))
        self.assertTrue(addr.is_anon)

    def test_command(self):
        out = StringIO()
        err = StringIO()
        opts = {'username': self.username}

        call_command('checkifexisted', **opts, stdout=out, stderr=err)
        self.assertTrue(out.getvalue().endswith('did not exist in database\n'))
        self.assertTrue(not err.getvalue())

        self.user.anonymize()
        call_command('checkifexisted', **opts, stdout=out, stderr=err)
        self.assertTrue(out.getvalue().endswith('existed in database\n'))
        self.assertTrue(not err.getvalue())

        call_command('checkifexisted', stdout=out, stderr=err)
        self.assertTrue(err.getvalue())
