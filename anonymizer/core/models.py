from enum import Enum

from django.contrib.auth import get_user_model
from django.db import models

from .abstract import AnonymousModel, FullyDeletableModel

User = get_user_model()


class HouseType(Enum):
    HOUSE = "HOUSE"
    BLOCK = "BLOCK"

    @classmethod
    def choices(cls):
        return tuple((i.name, i.value) for i in cls)


class Message(FullyDeletableModel):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=50, null=True)
    text = models.CharField(max_length=255, null=True)


class Address(AnonymousModel):
    class Meta:
        verbose_name_plural = "addresses"

    to_clean = {'street_name', 'city'}
    to_anonymize = {'contact_mail'}

    user = models.OneToOneField(
        User,
        related_name="address",
        null=False,
        on_delete=models.CASCADE,
    )
    street_name = models.CharField(max_length=100, null=True)
    city = models.CharField(max_length=50, null=True)
    contact_mail = models.EmailField(null=True)
    house_type = models.CharField(max_length=20, null=True, choices=HouseType.choices())
