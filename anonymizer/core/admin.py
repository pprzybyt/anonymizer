from django.contrib import admin

from .models import Address, Message


class CoreAdmin(admin.ModelAdmin):
    readonly_fields = ('is_anon',)


admin.site.register(Address, CoreAdmin)
admin.site.register(Message, CoreAdmin)
