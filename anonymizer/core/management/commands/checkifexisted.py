from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from anonymizer.core.encrypt import Encryptor

User = get_user_model()


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('-u', '--username', type=str, help='Define an username')
        parser.add_argument('-e', '--email', type=str, help='Define an email')

    def handle(self, *args, **options):
        username = options['username']
        email = options['email']

        if not any((username, email)):
            self.stderr.write('You have to pass at lest one of [user, main] to run the command properly')

        encryptor = Encryptor()
        users = User.objects.filter(is_anon=True)
        for field, check in [('username', username), ('email', email)]:
            if not check:
                continue
            for u in users:
                if encryptor.decrypt(getattr(u, field)) == check:
                    self.stdout.write(f'User with {field}: `{check}` existed in database')
                    break
            else:
                self.stdout.write(f'User with {field}: `{check}` did not exist in database')
