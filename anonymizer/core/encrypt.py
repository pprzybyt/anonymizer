from django.conf import settings

from Crypto import Random
from Crypto.Cipher import AES


class Encryptor:

    def __init__(self, key: str = None):
        key = key or settings.SECRET_KEY
        assert len(key) >= 32, 'Key used fpr encryption has to be at lest 32 char long'
        key = bytes(key[:32], encoding='utf-8')

        self.iv = Random.new().read(AES.block_size)
        self.encryptor = AES.new(key, AES.MODE_CFB, self.iv)
        self.decryptor = AES.new(key, AES.MODE_CFB, self.iv)

    def encrypt(self, text: str):
        data = self.iv + self.encryptor.encrypt(bytes(text, encoding='utf-8'))
        return data.hex()

    def decrypt(self, text: str):
        text = bytes.fromhex(text)
        return self.decryptor.decrypt(text)[len(self.iv):].decode("utf-8")
