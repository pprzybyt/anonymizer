from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'anonymizer.core'
