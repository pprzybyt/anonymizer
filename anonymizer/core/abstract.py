from typing import Set

from django.db import models

from .encrypt import Encryptor


class AnonymousModel(models.Model):
    class Meta:
        abstract = True

    fully_anonymize: bool = False
    to_clean: Set = {}
    to_anonymize: Set = {}

    is_anon = models.BooleanField(default=False)

    @property
    def _fields(self) -> Set:
        return set(field.name for field in self._meta.get_fields() if not field.name.endswith('_ptr')) - {'id'}

    def anonymize(self, parent: models.Model = None):
        if self.fully_anonymize:
            self.delete()
            return

        for field in self._meta.get_fields():
            name, model = field.name, field.related_model
            if not model:
                if name in self.to_anonymize:
                    setattr(self, name, self._encrypt(getattr(self, name)))
                elif name in self.to_clean:
                    setattr(self, name, None)
                continue

            if not issubclass(model, AnonymousModel) or model == parent.__class__:
                continue

            related_name = field.remote_field.name

            for obj in field.related_model.objects.filter(**{related_name: self}):
                obj.anonymize(parent=self)
        self.is_anon = True
        self.save()

    @staticmethod
    def _encrypt(text: str):
        text = text or ''
        return Encryptor().encrypt(text)


class FullyDeletableModel(AnonymousModel):
    fully_anonymize = True

    class Meta:
        abstract = True
