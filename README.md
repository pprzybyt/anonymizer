# User anonymizer project

---

## Requirements:
- `docker-compose` version: 1.24.0 (older probably too)
- `git`

---

## Instalation:

clone repository:
> git clone https://gitlab.com/pprzybyt/anonymizer.git

go to project dir:
>cd anonymizer

fill in env file (could stay as it is)
> cp `.env.example.local` `.env`

build and run:
> docker-compose -f local.yml up --build

---

## Usage:
- after running the `docker-compose up`, the app will be fed with startup data
- admin panel is now available under `http://0.0.0.0:8000/admin/` in your browser
- you can easily log into admin panel using default credentials (username: `admin`, password: `admin`)
- you can then view available models
- you can force user anonymization using django admin panel `action` (look for: `Anonymize user and corresponding tables` in users section)
- after that appropriate fields will be encrypted/removed or left as they were
- you can then use the command `checkifexisted` to check if a user with given `username`/`email` existed in the database (this works only for anonymized users)


## Useful commands:
- checking if given user existed (example):
> docker-compose -f local.yml up -d
> 
>docker-compose -f local.yml exec web /app/manage.py checkifexisted --email myemail@example.com --username admin

- running tests:
> docker-compose -f local.yml up -d
> 
> docker-compose -f local.yml exec web /app/manage.py test


## Additional info:

If You have any questions regarding that project, please catch me via pprzybyt@gmail.com
